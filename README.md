# sappy

Sappy is a simple bash script to build packages with squirrel

# How to use

```
Usage:
    sappy setup
    sappy make <name>
```

As you can see, sappy only provides two commands which are very easy to use.
The **setup** command sets up a basic toolchain to build packages in the current directory.
The **make** command compiles a **synced package** in this toolchain.

What do I mean by **synced package** ?
If you want to compile a package with sappy, it is needed to download or write the package file in your chosen branch path. If you are using Stock Linux as a host system, the package may already be synced because you have installed it on your system.

# A guide to write recipes

Each recipe must have the following fields in it: name, version, description, url (it is the homepage of the package), source, author, packager, makedeps (optional field, which is a list of required dependencies to compile the package), build (multiline field), post (optional multiline field, used as a post-install script).

To understand how a recipe should be written, this is a real model of a recipe:
```
name=attr
version=2.5.1
description=Commands for Manipulating Filesystem Extended Attributes.
url=https://savannah.nongnu.org/projects/attr/
source=http://download.savannah.nongnu.org/releases/attr/attr-2.5.1.tar.xz
author=Andreas Gruenbacher, Brandon Philips and Mike Frysinger
packager=Skythrew
build=(

./configure --prefix=/usr     \
            --disable-static  \
            --sysconfdir=/etc \
            --docdir=/usr/share/doc/attr-2.5.1

make

make DESTDIR=$PKG install

)
```

As you can see, there is no **makedeps field** in this recipe because attr doesn't have any compilation dependency.

The acl package depends on attr so this is its recipe:
```
name=acl
version=2.3.1
description=Commands for Manipulating POSIX Access Control Lists.
url=http://savannah.nongnu.org/projects/acl/
source=http://download.savannah.nongnu.org/releases/acl/acl-2.3.1.tar.xz
author=Andreas Gruenbacher, Brandon Philips and Mike Frysinger
packager=Skythrew
makedeps=attr
build=(

./configure --prefix=/usr         \
            --disable-static      \
            --docdir=/usr/share/doc/acl-2.3.1

make

make DESTDIR=$PKG install

)
```