#!/bin/bash

# Command line parser

if [ $UID != 0 ]
then
    echo -e "\033[0;31msappy must be run as root. Aborting.\033[0m"
    exit 1
fi

if [ "$1" != "setup" -a "$1" != "make" ]
then
    echo "Incorrect usage of sappy."
    echo "Usage:"
    echo "    sappy setup"
    echo "    sappy make <name>"
fi

if [ "$1" == "setup" ]
then
    LFS=$PWD
    mkdir -pv $LFS/{etc,var} $LFS/usr/{bin,lib,sbin}

    for i in bin lib sbin; do
        ln -sv usr/$i $LFS/$i
    done

    case $(uname -m) in
        x86_64) mkdir -pv $LFS/lib64 ;;
    esac

    mkdir -pv $LFS/{boot,home,mnt,opt,srv}

    mkdir -pv $LFS/etc/{opt,sysconfig}
    mkdir -pv $LFS/lib/firmware
    mkdir -pv $LFS/media/{floppy,cdrom}
    mkdir -pv $LFS/usr/{,local/}{include,src}
    mkdir -pv $LFS/usr/local/{bin,lib,sbin}
    mkdir -pv $LFS/usr/{,local/}share/{color,dict,doc,info,locale,man}
    mkdir -pv $LFS/usr/{,local/}share/{misc,terminfo,zoneinfo}
    mkdir -pv $LFS/usr/{,local/}share/man/man{1..8}
    mkdir -pv $LFS/var/{cache,local,log,mail,opt,spool}
    mkdir -pv $LFS/var/lib/{color,misc,locate}

    ln -sfv ../run $LFS/var/run
    ln -sfv ../run/lock $LFS/var/lock

    install -dv -m 0750 $LFS/root
    install -dv -m 1777 $LFS/tmp $LFS/var/tmp

    cp /etc/resolv.conf etc/
    cp /etc/squirrel.conf etc/
    cat > etc/hosts << EOF
127.0.0.1  localhost \$(hostname)
::1        localhost
EOF
    cat > etc/passwd << "EOF"
root:x:0:0:root:/root:/bin/bash
bin:x:1:1:bin:/dev/null:/usr/bin/false
daemon:x:6:6:Daemon User:/dev/null:/usr/bin/false
messagebus:x:18:18:D-Bus Message Daemon User:/run/dbus:/usr/bin/false
systemd-journal-gateway:x:73:73:systemd Journal Gateway:/:/usr/bin/false
systemd-journal-remote:x:74:74:systemd Journal Remote:/:/usr/bin/false
systemd-journal-upload:x:75:75:systemd Journal Upload:/:/usr/bin/false
systemd-network:x:76:76:systemd Network Management:/:/usr/bin/false
systemd-resolve:x:77:77:systemd Resolver:/:/usr/bin/false
systemd-timesync:x:78:78:systemd Time Synchronization:/:/usr/bin/false
systemd-coredump:x:79:79:systemd Core Dumper:/:/usr/bin/false
uuidd:x:80:80:UUID Generation Daemon User:/dev/null:/usr/bin/false
systemd-oom:x:81:81:systemd Out Of Memory Daemon:/:/usr/bin/false
nobody:x:65534:65534:Unprivileged User:/dev/null:/usr/bin/false
EOF
    cat > etc/group << "EOF"
root:x:0:
bin:x:1:daemon
sys:x:2:
kmem:x:3:
tape:x:4:
tty:x:5:
daemon:x:6:
floppy:x:7:
disk:x:8:
lp:x:9:
dialout:x:10:
audio:x:11:
video:x:12:
utmp:x:13:
usb:x:14:
cdrom:x:15:
adm:x:16:
messagebus:x:18:
systemd-journal:x:23:
input:x:24:
mail:x:34:
kvm:x:61:
systemd-journal-gateway:x:73:
systemd-journal-remote:x:74:
systemd-journal-upload:x:75:
systemd-network:x:76:
systemd-resolve:x:77:
systemd-timesync:x:78:
systemd-coredump:x:79:
uuidd:x:80:
systemd-oom:x:81:
wheel:x:97:
users:x:999:
nogroup:x:65534:
EOF

    mkdir -p var/log
    touch var/log/{btmp,lastlog,faillog,wtmp}
    chgrp -v utmp var/log/lastlog
    chmod -v 664  var/log/lastlog
    chmod -v 600  var/log/btmp

    mkdir -pv $PWD/{dev,proc,sys,run}
    squirrel sync --root=$PWD/
    squirrel install kernel-headers binutils m4 ncurses bash coreutils diffutils file findutils gawk grep gzip make patch sed tar xz gettext bison perl python texinfo systemd squirrel meson ninja pkg-config --root=$PWD/
    echo "Setup has been done successfully."
    echo "You are now ready to make packages !"

elif [ "$1" == "make" ]
then
    squirrel sync --root=$PWD/
    input="/etc/squirrel.conf"
    found=0
    while IFS= read -r line
    do
        if [[ $1 != "ARCH=*" ]]
        then
            branch=$(echo $line | cut -d " " -f 1)
            path=$(echo $line | cut -d " " -f 3)
            mkdir -p $PWD/$path/bins
            cp $path/.* $PWD/$path/
            if test -f "$path/$2"
            then
                if [ $found == 1 ]
                then
                    break
                fi
                found=1
                mkdir -p $PWD/$path/bins
                cp $path/INDEX $PWD/$path/
                cp $path/$2 $PWD/$path/
                # mkdir -p $PWD/$path
                # cp $path/$2 $PWD/$path/
                cmd="JOBS=$JOBS squirrel make $2"
                echo $cmd
                mount -v --bind /dev $PWD/dev
                mount -v --bind /dev/pts $PWD/dev/pts
                mount -vt proc proc $PWD/proc
                mount --rbind /sys $PWD/sys
                mount --make-rslave $PWD/sys
                mount -vt tmpfs tmpfs $PWD/run
                chroot $PWD /bin/bash <<EOF
JOBS=$JOBS squirrel make $2
EOF
                umount -R *
                cp $PWD/$path/$2 $path/
                cp $PWD/$path/.$2_TREE $path/
                mkdir -p $path/bins/
                cp $PWD/$path/bins/$2*.tar.xz $path/bins/
            fi
        fi
    done < "$input"
    if [ $found == 0 ]
    then
        echo -e "\033[0;31m$2 package file seems to not exist. Aborting.\033[0m"
        exit 1
    fi
fi